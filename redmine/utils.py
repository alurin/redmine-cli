

class Table:
    ''' Class for render console tables '''
    ALIGN_LEFT = 1
    ALIGN_CENTER = 2
    ALIGN_RIGHT = 3

    def __init__(self):
        self.columns = []
        self.rows = []
        self.formated = []

    def append_column(self, format='%s', align=ALIGN_LEFT, type=str, max_length=100):
        self.columns.append({
            'format': format,
            'align': align,
            'type': type,
            'max_length': max_length,
            'length': 0,
        })

    def append_row(self, *args):
        self.rows.append(args)

    def render(self):
        for row in self.rows:
            formated = []
            for i in range(0, len(row) - 1):
                value = self.columns[i]['type'](row[i])
                value = self.columns[i]['format'] % value
                formated.append(value)
                self.columns[i]['length'] = max(self.columns[i]['length'], len(value))
            self.formated.append(formated)

        for row in self.formated:
            print (*row)

