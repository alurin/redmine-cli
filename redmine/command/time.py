from redmine.command import Command
import requests
import json


class CreateTimeCommand(Command):
    def _insert_param(self, tag, item, arguments, param=None):
        if param == None:
            param = tag

        if getattr(arguments, param, None):
            item[tag] = getattr(arguments, param)

    def construct_json(self, arguments):
        time_entry = {}

        self._insert_param('issue_id', time_entry, arguments, param='issue')
        self._insert_param('project_id', time_entry, arguments, param='project')

        self._insert_param('hours', time_entry, arguments, param='hours')
        self._insert_param('activity_id', time_entry, arguments, param='activity')
        self._insert_param('comments', time_entry, arguments)

        # time_entry (required): a hash of the time entry attributes, including:
        #     issue_id or project_id (only one is required): the issue id or project id to log time on
        #     spent_on: the date the time was spent (default to the current date)
        #     hours (required): the number of spent hours
        #     activity_id: the id of the time activity. This parameter is required unless a default activity is defined in Redmine.
        #     comments: short description for the entry (255 characters max)

        return json.dumps({
            'time_entry': time_entry
        })

    def __call__(self, arguments, issue=None, project=None, **kwargs):
        if not issue and not project:
            raise RuntimeError('Project or issue is not setted')
        elif issue and project:
            raise RuntimeError('Project or issue is not setted')


        resource_url = self.construct_url('time_entries.json')
        headers = self.construct_headers()
        params = self.construct_json(arguments)

        response = requests.post(resource_url, headers=headers, data=params)
        if response.status_code == 201:
            # issue = response.json()['issue']
            # has_id = getattr(arguments, 'id', None)
            # if has_id:
            #     print(issue['id'])
            #     return True
            # else:
            print('Time for issue #%d is appended' % issue)
            return True

        print('Fatal error on issue create [Code: %d]' % response.status_code)
        return False
