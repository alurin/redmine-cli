from redmine.command import Command
from redmine.utils import Table
import requests
import json


class ListIssueCommand(Command):
    def __call__(self, arguments, **kwargs):
        resource_url = self.construct_limit(self.construct_url('/issues.json'), arguments)
        headers = self.construct_headers()
        params = {}

        if getattr(arguments, 'project', None):
            resource_url = self.construct_query(resource_url, project_id=arguments.project)

        response = requests.get(resource_url, headers=headers)
        if response.status_code == 200:
            data = response.json()

            table = Table()
            table.append_column('[%s]', type=str, align=Table.ALIGN_LEFT)
            table.append_column('#%d', type=int)
            table.append_column('%s', type=str)

            for issue in data['issues']:
                table.append_row(issue['status']['name'], issue['id'], issue['subject'], issue['author']['name'])

            table.render()

                # print("[%s] #%d. %s [%s]" % (issue['status']['name'], issue['id'], issue['subject'], issue['author']['name']))
            # start = data['offset'] + 1
            # finish = start + data['limit']

            # print("Показаны проекты с %d по %d" % (start, finish))
            # i = start
            # for project in data['projects']:
            #     print("%d. %s [%d - %s]" % (i, project['name'], project['id'], project['identifier']))
            #     i += 1
            return True
        elif response.status_code == 404:
            print('Not found')
            return False

        print('Status:', response.status_code)
        import pdb; pdb.set_trace()
        return False


class ShowIssueCommand(Command):
    def __call__(self, id):
        resource_url = self.construct_url('/issues/%d.json?include=attachments' % id)
        headers = self.construct_headers()
        headers = global_headers.copy()

        response = requests.get(resource_url, headers=headers)
        if response.status_code == 200:
            issue = response.json()['issue']

            title = '#%d %s' % (issue['id'], issue['subject'])
            print (title)
            print ("=" * len(title))
            print (issue['description'])

            if len(issue['attachments']):
                print ("")
                print ("Связанные файлы")
                print ("---------------")

                i = 0
                for attachment in issue['attachments']:
                    i += 1
                    print(str(i) + ')', attachment['content_url'])

            return True
        elif response.status_code == 404:
            print('Not found')
            return False

        print('Status:', response.status_code)
        return False


class ProcessMixin:
    def _insert_param(self, tag, issue, arguments, param=None):
        if param == None:
            param = tag

        if getattr(arguments, param, None):
            issue[tag] = getattr(arguments, param)

    def construct_json(self, arguments):
        issue = {}

        self._insert_param('project_id', issue, arguments, param='project')
        self._insert_param('parent_issue_id', issue, arguments, param='parent')
        self._insert_param('subject', issue, arguments)
        self._insert_param('estimated_hours', issue, arguments)
        self._insert_param('description', issue, arguments)

        # issue - A hash of the issue attributes:
        #     project_id
        #     tracker_id
        #     status_id
        #     priority_id
        #     subject
        #     description
        #     category_id
        #     fixed_version_id - ID of the Target Versions (previously called 'Fixed Version' and still referred to as such in the API)
        #     assigned_to_id - ID of the user to assign the issue to (currently no mechanism to assign by name)
        #     parent_issue_id - ID of the parent issue
        #     custom_fields - See Custom fields
        #     watcher_user_ids - Array of user ids to add as watchers (since 2.3.0)

        return json.dumps({
            'issue': issue
        })


class CreateIssueCommand(ProcessMixin, Command):
    def __call__(self, arguments, **kwargs):
        resource_url = self.construct_url('issues.json')
        headers = self.construct_headers()
        params = self.construct_json(arguments)

        response = requests.post(resource_url, headers=headers, data=params)
        if response.status_code == 201:
            issue = response.json()['issue']
            has_id = getattr(arguments, 'id', None)
            if has_id:
                print(issue['id'])
                return True
            else:
                print('Issue #%d is created' % issue['id'])
                return True
        print('Fatal error on issue create [Code: %d]' % response.status_code)
        return False


class UpdateIssueCommand(ProcessMixin, Command):
    def __call__(self, id, arguments, **kwargs):
        resource_url = self.construct_url('issues/%d.json' % id)
        headers = self.construct_headers()
        params = self.construct_json(arguments)

        response = requests.put(resource_url, headers=headers, data=params)

        if response.status_code == 200:
            print('Issue #%d is updated', id)
            return True
        elif response.status_code == 404:
            print('Issue #%d not found', id)
            return False

        print('Fatal error on issue #%d update [Code: %d]' % (id, response.status_code))
        return False
