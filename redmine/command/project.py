from redmine.command import Command
import requests
import json


class ListProjectCommand(Command):
    def __call__(self, arguments, **kwargs):
        resource_url = self.construct_limit(self.construct_url('/projects.json'), arguments)
        headers = self.construct_headers()

        response = requests.get(resource_url, headers=headers)
        if response.status_code == 200:
            data = response.json()

            start = data['offset'] + 1
            finish = start + data['limit']

            print("Показаны проекты с %d по %d" % (start, finish))
            i = start
            for project in data['projects']:
                print("%d. %s [%d - %s]" % (i, project['name'], project['id'], project['identifier']))
                i += 1
            return True
        elif response.status_code == 404:
            print('Not found')
            return False

        print('Status:', response.status_code)
        return False
