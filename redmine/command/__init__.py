import urllib
import urllib.parse as urlparse

class Command:
    def __init__(self, settings):
        self.settings = settings

    def __call__(self, args):
        raise NotImplementedError('Command executing is not implemented')

    def construct (self):
        pass

    def construct_url(self, url):
        return urlparse.urljoin(self.settings.API_URL, url)

    def construct_query(self, url, **params):
        # Append to URL query
        url_parts = list(urlparse.urlparse(url))
        query = dict(urlparse.parse_qsl(url_parts[4]))
        query.update(params)
        url_parts[4] = urlparse.urlencode(query)
        return urlparse.urlunparse(url_parts)

    def construct_limit(self, url, arguments):
        params = {
            'offset': (arguments.page - 1) * arguments.count,
            'limit': arguments.count,
        }
        return self.construct_query(url, **params)


    def construct_headers(self, append_headers={}):
        headers = append_headers.copy()
        headers.update({
            'content-type': 'application/json',
            'X-Redmine-API-Key': self.settings.API_KEY
        })
        return headers
