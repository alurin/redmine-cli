import os
import configparser

# Load local settings
CONFIG_PATH = os.path.join(os.environ.get('HOME'), '.config/redmine-cli.conf')

config = configparser.ConfigParser()
config.read(CONFIG_PATH)

# Scheme for Redmine API server
API_HTTPS = config.getboolean('server', 'https', fallback=False)

# Host for Redmine API server
API_HOST = config.get('server', 'hostname')

# Full hostname and scheme for Redmine API server
API_URL = ('https' if API_HTTPS else 'http') + '://' + API_HOST

# User api key
API_KEY  = config.get('server', 'api_key')
