# PYTHON_ARGCOMPLETE_OK

import sys
import argparse
import argcomplete
from redmine import settings

from redmine.command.issue import ListIssueCommand
from redmine.command.issue import ShowIssueCommand
from redmine.command.issue import UpdateIssueCommand
from redmine.command.issue import CreateIssueCommand

from redmine.command.project import ListProjectCommand

from redmine.command.time import CreateTimeCommand


def create_project_command(subparsers):
    # create the parser for the "a" command
    project_parser = subparsers.add_parser('project', help='Управление проектами')
    project_subparsers = project_parser.add_subparsers()

    # Задача
    list_parser = project_subparsers.add_parser('list', help='Список проектов')
    list_parser.add_argument('--page', type=int, default=0, required=False,)
    list_parser.add_argument('--count', type=int, default=10, required=False,)
    list_parser.add_argument('_command', help=argparse.SUPPRESS, action='store_const', const=ListProjectCommand)


def create_issue_command(subparsers):
    # create the parser for the "a" command
    issue_parser = subparsers.add_parser('issue', help='Управление задачами')
    issue_subparsers = issue_parser.add_subparsers()

    # Задача
    list_parser = issue_subparsers.add_parser('list', help='Список задач')
    list_parser.add_argument('--page', type=int, default=0, required=False,)
    list_parser.add_argument('--count', type=int, default=10, required=False,)
    list_parser.add_argument('-p', '--project', type=int, required=True,)
    list_parser.add_argument('_command', help=argparse.SUPPRESS, action='store_const', const=ListIssueCommand)

    # Задача
    show_parser = issue_subparsers.add_parser('show', help='Просмотр текста задачи')
    show_parser.add_argument('id', metavar='ID', type=int,)
    show_parser.add_argument('_command', help=argparse.SUPPRESS, action='store_const', const=ShowIssueCommand)

    # Задача
    update_parser = issue_subparsers.add_parser('update', help='Список задач')
    update_parser.add_argument('id', metavar='ID', type=int,)
    update_parser.add_argument('-m', '--subject', nargs='?', type=str,)
    update_parser.add_argument('--description', nargs='?', type=str,)
    update_parser.add_argument('--estimated-hours', nargs='?', type=float,)
    update_parser.add_argument('--parent', nargs='?', type=int,)
    update_parser.add_argument('_command', help=argparse.SUPPRESS, action='store_const', const=UpdateIssueCommand)

    # Задача
    create_parser = issue_subparsers.add_parser('create', help='Список задач')
    create_parser.add_argument('-p', '--project', type=str, required=True,)
    create_parser.add_argument('-m', '--subject', type=str, required=True,)
    create_parser.add_argument('--description', nargs='?', type=str,)
    create_parser.add_argument('--estimated-hours', nargs='?', type=float,)
    create_parser.add_argument('--parent', nargs='?', type=int,)
    create_parser.add_argument('--id', action='store_true')
    create_parser.add_argument('_command', help=argparse.SUPPRESS, action='store_const', const=CreateIssueCommand)


def create_time_command(subparsers):
    # create the parser for the "a" command
    time_parser = subparsers.add_parser('time', help='Управление временем')
    issue_subparsers = time_parser.add_subparsers()

    # Задача
    create_parser = issue_subparsers.add_parser('create', help='Список задач')

    create_parser.add_argument('-p', '--project', nargs='?', type=int,)
    create_parser.add_argument('-i', '--issue', nargs='?', type=int,)
    create_parser.add_argument('-a', '--activity', nargs='?', type=int,)
    create_parser.add_argument('hours', type=float,)
    create_parser.add_argument('--comments', nargs='?', type=str,)
    create_parser.add_argument('_command', help=argparse.SUPPRESS, action='store_const', const=CreateTimeCommand)


def create_parser():
    parser = argparse.ArgumentParser(description='Работа с Redmine через Rest API.')

    # create the top-level parser
    subparsers = parser.add_subparsers(help='Команды')

    # create top-level commandscommands
    create_issue_command(subparsers)
    create_project_command(subparsers)
    create_time_command(subparsers)

    return parser

def main():
    '''
    Main point for console Redmine client
    '''
    parser = create_parser()

    # Parse arguments
    argcomplete.autocomplete(parser)
    arguments = parser.parse_args()

    # If command found, execute this
    if '_command' in arguments:
        clazz = arguments._command
        args = arguments
        arguments = vars(arguments)
        del arguments['_command']
        instance = clazz(settings)
        if instance(arguments=args, **arguments):
            sys.exit(0)
        else:
            sys.exit(1)
    else:
        print('Command not found')
