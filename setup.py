#!/usr/bin/env python3
'''
Copyright (C) Vasiliy Sheredeko - All Rights Reserved

Unauthorized copying of this file, via any medium is strictly prohibited
Proprietary and confidential
Written by Vasiliy Sheredeko <vasiliy@sheredeko.me>, December 2013
'''

from setuptools import setup, find_packages
import redmine

setup(
    name="redmine-cli",
    version=redmine.__version__,
    author="Vasiliy Sheredeko <vasiliy@sheredeko.me>",
    license="Copyright 2013-2014 (C) Vasiliy Sheredeko",
    description="",
    packages=find_packages(),
    data_files=[
        # ('/etc/init.d', ['init-script'])
    ],
    entry_points={
        'console_scripts': [
            'redmine-cli = redmine.cli:main',
        ],
    },
    install_requires=[
        'requests == 2.2.0',
        'argcomplete == 0.7.0',
    ],
    zip_safe=False
)
